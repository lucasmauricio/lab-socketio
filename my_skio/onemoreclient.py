"""
Reference: https://pypi.org/project/socketIO-client/
"""
from socketIO_client import SocketIO, BaseNamespace

class Namespace(BaseNamespace):

    def on_connect(self):
        print('[Connected]')

    def on_reconnect(self):
        print('[Reconnected]')

    def on_disconnect(self):
        print('[Disconnected]')

socketIO = SocketIO('localhost', 9000, Namespace)
socketIO.wait(seconds=1)
