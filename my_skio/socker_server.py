"""
https://stackoverflow.com/questions/4762086/socket-io-client-library-in-python

https://pypi.org/project/socketIO-client/
"""

from socketIO_client import SocketIO, BaseNamespace

class Namespace(BaseNamespace):

    def on_aaa_response(self, *args):
        print('on_aaa_response', args)
        self.emit('bbb')

socketIO = SocketIO('localhost', 9009)
socketIO.define(Namespace)
socketIO.on('aaa')
socketIO.wait(seconds=1)
