"""
Reference:
https://github.com/uiot/raise-p2p/

https://flask-socketio.readthedocs.io/en/latest/


TODO read https://stackoverflow.com/questions/4762086/socket-io-client-library-in-python
about using websocket.
"""
from flask import Flask
from flask_socketio import SocketIO, emit
import socket

app = Flask(__name__)
async_mode = None
socketio = SocketIO(app, async_mode=async_mode)

class SocketServer:

    def __init__(self, host_ip, socketio):
        self.HOST = host_ip
        self.PORT = 12575
        self.stop = False
        self.socketio = socketio
        self.socketio.emit('message', {'data': 'Peer iniciado.'},
                            namespace='/message')
        self.init_socket()

    def init_socket(self):
        """Start TCP server socket on port 12575.
        Queue up 10 connect requests before refusing outside connections.
        """
        #
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # SO_REUSEADDR : the port 12575 will be immediately reusable after
        # the socket is closed
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.HOST, self.PORT))
        self.socket.listen(10)
        self.socketio.emit('message', {'data': 'Socket iniciado.'}, namespace='/message')


@socketio.on('test_con', namespace='/message')
def ping_mesage(data_rcv):
    print(msg)
    # socketio.emit('message', {'data': msg}, namespace='/message')

if __name__ == '__main__':
    #TODO get the IP address from OS
    ip = 'localhost'
    server = SocketServer(ip, socketio)
    # server.init_socket()
