"""
https://stackoverflow.com/questions/4762086/socket-io-client-library-in-python

https://pypi.org/project/socketIO-client/
"""

from socketIO_client import SocketIO

def on_bbb_response(*args):
    print('on_bbb_response', args)

with SocketIO('localhost', 9009) as socketIO:
    socketIO.emit('bbb', {'xxx': 'yyy'}, on_bbb_response)
    # socketIO.wait_for_callbacks(seconds=1)
print("we're done")
