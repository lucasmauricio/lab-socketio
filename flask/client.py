"""
from https://stackoverflow.com/questions/46938320/python-socketio-client
"""
from socketIO_client import SocketIO, LoggingNamespace
import socket
import logging

logging.getLogger('requests').setLevel(logging.WARNING)
logging.basicConfig(level=logging.DEBUG)

def on_online():
    print ('online')

socket = SocketIO('127.0.0.1', 5000, LoggingNamespace)

message=''

while (message!='quit'):
    socket.on('message', on_online)
