from flask import Flask, render_template
from flask_socketio import SocketIO

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

@socketio.on('message')
def handle_message(message):
    print(f"Mensagem recebida: {message}")
    send(f"I got '{message}' from you.")

if __name__ == '__main__':
    socketio.run(app)
