# Python SocketIO Lab

This is an experimental project to develop some reactive programs using the SocketIO library.

By the way, I suggest to use [VirtualEnv](https://virtualenv.pypa.io/) for running it without changing your system configuration.
The file `requirements.txt` describes all projects' dependencies.
